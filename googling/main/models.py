from django.db import models
import datetime
import uuid
# Create your models here.


class Link(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created = models.DateTimeField(default=datetime.datetime.utcnow)
    search_query = models.TextField(default="")
    title = models.TextField(default="")
    url = models.TextField(default="")
