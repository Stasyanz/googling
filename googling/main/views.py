from django.shortcuts import render, HttpResponse, render_to_response, HttpResponseRedirect
from requests_html import HTMLSession
from .models import Link
import re
# Create your views here.


mapping = {
    "title": ".//h3",
    "url": ".//a/@href",
}


g_ads_mapping = "//div[@id='tads']"


def parse_url(value):
    r = re.compile("(.+?)(?===)", re.I)
    v = r.search(value).groups()
    return v[0]


def parse_title(value):
    r = re.compile("\=\=(.*)", re.I)
    v = r.search(value).groups()
    return v[0]


def get_content(page, query):
    session = HTMLSession()
    q = {"q": query, "start": page*10}
    response = session.get(url="https://google.com/search", params=q)
    return response


def parse_page(response, mapping):
    if response:
        q = "//div[contains(@id, 'center_col')]"
        content = response.html.xpath(q, first=True)
        items = content.xpath(mapping)
        return items


def parse_item(item):
    d = dict()
    d["title"] = item.xpath(mapping["title"], first=True).text
    d["url"] = item.xpath(mapping["url"], first=True)
    return d


def find_results(query, pages, ads):
    pages = pages
    query = query
    items_list = list()
    responses = list()
    for page in range(pages):
        response = get_content(page=page, query=query)
        if response:
            responses.append(response)
    for response in responses:
        items_ads = parse_page(response, "//div[@id='tads']/ol/li")
        items_ads_bottom = parse_page(response, "//div[@id='tadsb']/ol/li")
        items_regular = parse_page(response, "//div[@class='g']")
        if ads:
            for item in items_ads:
                ad = parse_item(item)
                items_list.append(ad)
            for item in items_ads_bottom:
                adb = parse_item(item)
                items_list.append(adb)
            for item in items_regular:
                reg = parse_item(item)
                items_list.append(reg)
        else:
            for item in items_regular:
                r = parse_item(item)
                items_list.append(r)
    return items_list


def main(request, template="main.html"):
    return render_to_response(template_name=template)


def search(request):
    if all(["query" in request.GET, "pages" in request.GET]):
        query = request.GET['query']
        pages = int(request.GET["pages"])
        if len(query) > 0:
            if "ads" in request.GET:  # need to parse ads too
                results = find_results(query, pages, ads=True)
                return render_to_response("results.html", {"query": query, "results": results})
            else:
                results = find_results(query, pages, ads=False)
                return render_to_response("results.html", {"query": query, "results": results})
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


def save(request, template="done.html"):
    if request.method == "POST":
        saved = 0
        search_query = request.POST["query"]
        for key, value in request.POST.items():
            if key == "query":
                pass
            else:
                new_record = Link()
                url = parse_url(value)
                title = parse_title(value)
                new_record.url = url
                new_record.search_query = search_query
                new_record.title = title
                new_record.save()
                saved += 1
                context = {"saved": saved}
        return render_to_response(template, context)
    pass
