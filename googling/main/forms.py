from django import forms
from django.forms import ModelForm
from .models import Link


class LinkForm(forms.Form):
    # class Meta:
    #     model = Link
    title = forms.CharField()
    url = forms.URLField()
    checked = forms.CheckboxInput()
