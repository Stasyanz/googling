This project takes google search result and allows saving selected results to local database for further usage

# Installation and using

1. `git clone https://Stasyanz@bitbucket.org/Stasyanz/googling.git`
2. `cd googling/googling`
3. `pip install requirements.txt`
4. `python manage.py makemigrations`
5. `python manage.py makamigrations main`
6. `python manage.py migrate`
7. `python manage.py runserver`
8. Navigate to `http://127.0.0.1:8000`
9. Do your stuff

## Setting up a database

By default `sqlite` is used. The sqlite file is created in the project directory and has a name `ds.sqlie`.
To use `Postgres` or `MySql` you need to edit `googling/settings.py` file's `DATABASES` section.
There are commented templates to use. Just comment the sqlite section and set up your database settings.
You can use [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-use-mysql-or-mariadb-with-your-django-application-on-ubuntu-14-04) 
to set up MySql
